//
//  Campaign.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/29/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import "Campaign.h"

@implementation Campaign
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.id_campaign = [decoder decodeObjectForKey:@"id_campaign"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.subline = [decoder decodeObjectForKey:@"subline"];

        self.desc = [decoder decodeObjectForKey:@"desc"];
        self.url_key = [decoder decodeObjectForKey:@"url_key"];
        self.start_time = [decoder decodeObjectForKey:@"start_time"];
        self.start_time_formatted = [decoder decodeObjectForKey:@"start_time_formatted"];
        self.end_time_formatted = [decoder decodeObjectForKey:@"end_time_formatted"];
        self.end_time = [decoder decodeObjectForKey:@"end_time"];
        self.banner_url = [decoder decodeObjectForKey:@"banner_url"];
        self.navigation_url = [decoder decodeObjectForKey:@"navigation_url"];
        self.navi_image = [decoder decodeObjectForKey:@"navi_image"];
        self.selected = [decoder decodeObjectForKey:@"selected"];
        self.preview_image1_url = [decoder decodeObjectForKey:@"preview_image1_url"];
        self.preview_image2_url = [decoder decodeObjectForKey:@"preview_image2_url"];
        self.preview_image3_url = [decoder decodeObjectForKey:@"preview_image3_url"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.id_campaign forKey:@"id_campaign"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.subline forKey:@"subline"];

    [encoder encodeObject:self.desc forKey:@"desc"];
    [encoder encodeObject:self.url_key forKey:@"url_key"];
    [encoder encodeObject:self.start_time forKey:@"start_time"];
    [encoder encodeObject:self.start_time_formatted forKey:@"start_time_formatted"];
    [encoder encodeObject:self.end_time_formatted forKey:@"end_time_formatted"];
    [encoder encodeObject:self.end_time forKey:@"end_time"];
    [encoder encodeObject:self.banner_url forKey:@"banner_url"];
    [encoder encodeObject:self.navigation_url forKey:@"navigation_url"];
    [encoder encodeObject:self.navi_image forKey:@"navi_image"];
    [encoder encodeObject:self.selected forKey:@"selected"];
    [encoder encodeObject:self.preview_image1_url forKey:@"preview_image1_url"];
    [encoder encodeObject:self.preview_image2_url forKey:@"preview_image2_url"];
    [encoder encodeObject:self.preview_image3_url forKey:@"preview_image3_url"];
}
@end
