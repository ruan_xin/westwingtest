//
//  SwipeTransition.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/30/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import "SwipeTransition.h"
@interface SwipeTransition()
@property(nonatomic, strong) UINavigationController *navigationController;
@end
@implementation SwipeTransition
-(void) attachToViewController:(UIViewController *)viewController;
{
    self.transitionInProgress = false;
    self.navigationController = viewController.navigationController;
    [self setupGestureRecognizer:viewController.view];
}

-(void) setupGestureRecognizer:(UIView *)view;
{
    UIScreenEdgePanGestureRecognizer *leftEdgeGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftEdgeGesture:)];
    leftEdgeGesture.edges = UIRectEdgeLeft;
    [view addGestureRecognizer:leftEdgeGesture];
}

-(void) handleLeftEdgeGesture:(UIPanGestureRecognizer *)gestureRecognizer;
{
    CGPoint viewTranslation = [gestureRecognizer translationInView:gestureRecognizer.view.superview];
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:
            self.transitionInProgress = true;
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case UIGestureRecognizerStateChanged:{
            CGFloat d = fabs(viewTranslation.x / CGRectGetWidth(gestureRecognizer.view.bounds));
            [self updateInteractiveTransition:d];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            self.transitionInProgress = false;
            if ([gestureRecognizer velocityInView:gestureRecognizer.view.superview].x > 0) {
                [self finishInteractiveTransition];
            } else {
                [self cancelInteractiveTransition];
            }
            break;
        default:
            break;
    }
}
@end
