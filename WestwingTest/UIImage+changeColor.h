//
//  UIImage+changeColor.h
//  ToDoList
//
//  Created by Xin Ruan on 14-3-20.
//  Copyright (c) 2014年 Jiajian Xiao. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UIImage (changeColor)
- (UIImage *)imageWithOverlayColor:(UIColor *)color;
- (UIImage *)imageWithOverlayColor:(UIColor *)color andFrame:(CGRect)rect;
- (UIImage *)scaledToSize:(CGSize)newSize;
- (UIImage *)crop:(CGRect)rect;
//- (UIImage *)cropMiddle;
- (UIImage *)fixOrientation;
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
