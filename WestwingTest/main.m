//
//  main.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/28/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
