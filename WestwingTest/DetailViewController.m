//
//  DetailViewController.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/30/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import "DetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "LoadingCircleView.h"
@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *previewScrollView;
@property (strong, nonatomic) NSMutableArray *previewImageURLArray;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sublineLabel;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBannerLoadingCircle];
    [self initBannerLoadingCircleConstraint];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupContent:(Campaign *)aCampain;
{
    dispatch_async(dispatch_get_main_queue(),^{
        self.nameLabel.text = aCampain.name;
        self.sublineLabel.text = aCampain.subline;
        NSMutableAttributedString *descText = [[NSMutableAttributedString alloc]initWithString:aCampain.desc];
        [descText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BrandonGrotesque-Bold" size:14] range:NSMakeRange(0, descText.length)];
        [descText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:171/255.0 green:171/255.0 blue:154/255.0 alpha:1.0] range:NSMakeRange(0, descText.length)];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentJustified];
        [descText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, descText.length)];

        self.descTextView.attributedText = descText;
        [self.bannerImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:aCampain.banner_url]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            [self hideBannerLoadingCircle];
            self.bannerImageView.image = image;
            //c.navi_image = image;
            [self.bannerImageView setNeedsLayout];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            NSLog(@"err %@",error);
        }];
        self.previewImageURLArray = [NSMutableArray array];
        if (aCampain.preview_image1_url) {
            [self.previewImageURLArray addObject:aCampain.preview_image1_url];
        }
        if (aCampain.preview_image2_url) {
            [self.previewImageURLArray addObject:aCampain.preview_image2_url];
        }
        if (aCampain.preview_image3_url) {
            [self.previewImageURLArray addObject:aCampain.preview_image3_url];
        }
        CGFloat x = 0;
        for (NSString *preview_url in self.previewImageURLArray) {
            x=[self initPreviewImageWithURL:preview_url withX:x];
            //index++;
        }
        CGSize sz = self.previewScrollView.bounds.size;
        sz.width = x;
        self.previewScrollView.contentSize = sz;

    });
}
- (CGFloat)initPreviewImageWithURL:(NSString *)url withX:(int)x;
{
    UIImageView *previewImageView = [UIImageView new];
    previewImageView.contentMode = UIViewContentModeScaleAspectFit;
    CGRect f = previewImageView.frame;
    f.origin = CGPointMake(x,0);
    f.size = CGSizeMake(self.view.frame.size.width/[self.previewImageURLArray count], 100);
    previewImageView.frame = f;
    [self.previewScrollView addSubview:previewImageView];
    [self downloadPreview:previewImageView WithURL:url];
    return x += previewImageView.bounds.size.width +5;
    
}
- (void)downloadPreview:(UIImageView *)previewImageView WithURL:(NSString *)url;
{
        __weak UIImageView *weakPreviewImageView = previewImageView;
        [weakPreviewImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            weakPreviewImageView.alpha = 0.0;
            weakPreviewImageView.image = image;
            NSLog(@"weakPreviewImageView frame %@", NSStringFromCGRect(weakPreviewImageView.frame));
            [UIView animateWithDuration:0.25
                             animations:^{
                                 weakPreviewImageView.alpha = 1.0;
                             }];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            NSLog(@"err %@",error);
        }];
}
- (void)showBannerLoadingCircle;
{
    if (![self.bannerImageView viewWithTag:101]) {
        LoadingCircleView *bannerLoadingCircleView = [[LoadingCircleView alloc] init];
        bannerLoadingCircleView.tag = 101;
        [self.bannerImageView addSubview:bannerLoadingCircleView];
        
        bannerLoadingCircleView.hidden = NO;

    }else{
        LoadingCircleView *bannerLoadingCircleView = (LoadingCircleView *)[self.bannerImageView viewWithTag:101];
        bannerLoadingCircleView.hidden = NO;
    }
}
- (void)hideBannerLoadingCircle;
{
    LoadingCircleView *bannerLoadingCircleView = (LoadingCircleView *)[self.bannerImageView viewWithTag:101];
    bannerLoadingCircleView.hidden = YES;
}
- (void)initBannerLoadingCircleConstraint;
{
    LoadingCircleView *bannerLoadingCircleView = (LoadingCircleView *)[self.bannerImageView viewWithTag:101];
    [self.bannerImageView addConstraint:[NSLayoutConstraint constraintWithItem:bannerLoadingCircleView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bannerImageView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0]];
    [self.bannerImageView addConstraint:[NSLayoutConstraint constraintWithItem:bannerLoadingCircleView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bannerImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0
                                                                      constant:0]];
}
- (void)initPreviewImageLoadingCircleConstraint;
{
    LoadingCircleView *bannerLoadingCircleView = (LoadingCircleView *)[self.bannerImageView viewWithTag:101];
    [self.bannerImageView addConstraint:[NSLayoutConstraint constraintWithItem:bannerLoadingCircleView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bannerImageView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0
                                                                      constant:0]];
    [self.bannerImageView addConstraint:[NSLayoutConstraint constraintWithItem:bannerLoadingCircleView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bannerImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0
                                                                      constant:0]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
