//
//  MainTableViewCell.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/29/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell/MGSwipeTableCell.h>
@interface MainTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sublineLabel;
@property (weak, nonatomic) IBOutlet UIImageView *naviImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UILabel *seTimeLabel;
- (void)showLoadingCircle;
- (void)hideLoadingCircle;
@end
