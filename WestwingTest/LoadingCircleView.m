//
//  CircleBG2View.m
//  MusicShare
//
//  Created by Xin on 01/04/15.
//  Copyright (c) 2015 unison. All rights reserved.
//

#import "LoadingCircleView.h"
@interface LoadingCircleView()
@property (nonatomic, strong) CAShapeLayer *circleLayer;

@end
@implementation LoadingCircleView

- (id)init
{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];

    self = [super init];
    if (self) {
        // Initialization code
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:50.0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:50.0]];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!self.circleLayer)
    {
        self.circleLayer = [CAShapeLayer layer];
        [self.layer addSublayer:self.circleLayer];
    }
    
    CGRect smallBounds = CGRectInset(self.bounds, 0, self.bounds.size.height/3.5);
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    CGFloat radius = MIN(smallBounds.size.height/2, smallBounds.size.width/2);
    UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:center radius:1.5*radius startAngle:0 endAngle:2*M_PI clockwise:YES];
    self.circleLayer.strokeColor = [UIColor grayColor].CGColor;
    self.circleLayer.lineWidth = 1.0;
    self.circleLayer.fillColor = [UIColor clearColor].CGColor;
    self.circleLayer.path = circle.CGPath;
    self.circleLayer.strokeStart = 0.1;
    self.circleLayer.lineCap = kCALineCapRound;
    self.circleLayer.lineJoin = kCALineJoinRound;
    self.circleLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    [self rotateLayer:self.circleLayer];
}

- (void)rotateLayer:(CALayer *)layer
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 3.0f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
}


@end
