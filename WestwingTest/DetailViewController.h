//
//  DetailViewController.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/30/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Campaign.h"
@interface DetailViewController : UIViewController

-(void)setupContent:(Campaign *)aCampain;
@end
