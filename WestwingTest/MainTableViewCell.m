//
//  MainTableViewCell.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/29/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import "MainTableViewCell.h"
#import "LoadingCircleView.h"

@interface MainTableViewCell()
@property (nonatomic, strong) LoadingCircleView *loadingCircleView;

@end
@implementation MainTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.layer.masksToBounds =NO;
    self.layer.borderWidth=0.5f;
    self.layer.borderColor=[UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1.0].CGColor;
    [self initLoadingCircleConstraint];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)showLoadingCircle;
{
    self.loadingCircleView.hidden = NO;
}
- (void)hideLoadingCircle;
{
    self.loadingCircleView.hidden = YES;
}
- (LoadingCircleView *)loadingCircleView;
{
    if (!_loadingCircleView) {
        _loadingCircleView = [[LoadingCircleView alloc] init];
        [self.naviImageView addSubview:_loadingCircleView];

    }
    return _loadingCircleView;
}
- (void)initLoadingCircleConstraint;
{
    [self.naviImageView addConstraint:[NSLayoutConstraint constraintWithItem:self.loadingCircleView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.naviImageView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0
                                                                      constant:0]];
    [self.naviImageView addConstraint:[NSLayoutConstraint constraintWithItem:self.loadingCircleView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.naviImageView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0
                                                                      constant:0]];
}
@end
