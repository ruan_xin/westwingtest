//
//  SwipeAnimation.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/30/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SwipeAnimation : NSObject <UIViewControllerAnimatedTransitioning>

@end
