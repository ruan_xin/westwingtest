//
//  MainTableViewController.m
//  WestwingTest
//
//  Created by Xin Ruan on 7/29/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import "MainTableViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Campaign.h"
#import "MainTableViewCell.h"
#import "SwipeTransition.h"
#import "SwipeAnimation.h"
#import "DetailViewController.h"
#import <MGSwipeTableCell/MGSwipeButton.h>
#import <EventKit/EventKit.h>
#import "UIImage+changeColor.h"
#define ICONCOLOR [UIColor colorWithRed:130/255.0 green:130/255.0 blue:111/255.0 alpha:1.0]
@interface MainTableViewController ()<UINavigationControllerDelegate, MGSwipeTableCellDelegate>
@property (nonatomic, strong) NSMutableArray *campaigns;
@property (nonatomic, strong) SwipeTransition *swipeTransition;
@property (nonatomic, strong) SwipeAnimation *swipeAnimation;
@property (nonatomic, strong) UILabel *loadingLabel;
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@end

@implementation MainTableViewController
{
    NSInteger selectedRow;
}
static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedRow = -1;
    [self campaigns];
    self.navigationController.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestCampain)
                  forControlEvents:UIControlEventValueChanged];
    [self getLatestCampain];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateBackgroundColor)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];

}
-(void)dealloc;
{
    
}
-(void)updateBackgroundColor;
{
    NSLog(@"updateBackground");
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    self.tableView.backgroundColor = color;

}
-(void)viewWillAppear:(BOOL)animated;
{
    [self updateBackgroundColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)reloadData;
{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSData *storedCampainData = [NSKeyedArchiver archivedDataWithRootObject:self.campaigns];
    [defaults setObject:storedCampainData forKey:@"storedCampain"];
    [defaults synchronize];
    [self.tableView reloadData];
}

- (void)getLatestCampain;
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    [manager GET:@"http://www.westwing.uk/data.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSMutableArray *filterName = [NSMutableArray new];
         NSMutableArray *filterID = [self getCampainID];
         if ([responseObject[@"success"] isEqualToNumber:[NSNumber numberWithBool:TRUE]]) {
             for (NSDictionary *item in responseObject[@"metadata"][@"upcoming"]) {
                 if ([filterName containsObject:item[@"name"]]) {
                     continue;
                 }else{
                     [filterName addObject:item[@"name"]];
                 }
                 
                 if ([filterID containsObject:item[@"id_campaign"]]) {
                     continue;
                 }
                 Campaign *c = [[Campaign alloc] init];
                 c.id_campaign = item[@"id_campaign"];
                 c.name = item[@"name"];
                 c.selected = @"no";
                 c.subline = item[@"subline"];
                 c.desc = item[@"description"];
                 c.url_key = item[@"url_key"];
                 c.start_time = item[@"start_time"];
                 c.start_time_formatted = item[@"start_time_formatted"];
                 c.end_time = item[@"end_time"];
                 c.end_time_formatted = item[@"end_time_formatted"];
                 c.navigation_url = item[@"navigation_url"];
                 c.banner_url = item[@"banner_url"];
                 c.preview_image1_url = item[@"preview_image1_url"];
                 c.preview_image2_url = item[@"preview_image2_url"];
                 c.preview_image3_url = item[@"preview_image3_url"];
                 [self.campaigns addObject:c];
                 c = nil;
             }
             [self reloadData];
         }else{
             UILabel *loadingLabel = (UILabel *)self.tableView.backgroundView;
             loadingLabel.text = @"We met some trouble connection server, please try pull down to reflesh later.";
         }
         [self.refreshControl endRefreshing];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         NSLog(@"%@", error);
         UILabel *loadingLabel = (UILabel *)self.tableView.backgroundView;
         loadingLabel.text = @"We met some trouble connection server, please try pull down to reflesh later.";
         [self.refreshControl endRefreshing];

     }];
}
-(NSMutableArray *)getCampainID
{
    NSMutableArray *filterID = [NSMutableArray new];
    for (Campaign *campain in self.campaigns) {
        [filterID addObject:campain.id_campaign];
    }
    return filterID;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSInteger count = self.campaigns.count;
    if (count == 0) {
        self.loadingLabel.hidden = NO;
    }else{
        self.loadingLabel.hidden = YES;
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    Campaign *c = [self.campaigns objectAtIndex:indexPath.row];
    cell.leftButtons = @[[MGSwipeButton buttonWithTitle:@"" icon:[[UIImage imageNamed:@"reminder"] imageWithOverlayColor:ICONCOLOR] backgroundColor:[UIColor clearColor]]];
    cell.leftSwipeSettings.transition = MGSwipeTransitionBorder;
    cell.delegate = self;
    // Configure the cell...
    cell.nameLabel.text = c.name;
    cell.sublineLabel.text = c.subline;
    cell.seTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", c.start_time_formatted, c.end_time_formatted];
    [cell showLoadingCircle];
    if (c.navigation_url != nil) {
        __weak MainTableViewCell *weakCell = cell;
        [cell.naviImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:c.navigation_url]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            [weakCell hideLoadingCircle];
            weakCell.naviImageView.image = image;
            c.navi_image = image;
            [weakCell setNeedsLayout];

        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
        }];
    }
    if ([c.selected isEqualToString:@"yes"]) {
        selectedRow = indexPath.row;
        cell.selectedImageView.hidden=NO;
    }else{
        cell.selectedImageView.hidden=YES;
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 124.0;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"doSegue"]) {
        NSLog(@"segue");
        MainTableViewCell *cell = (MainTableViewCell*)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        if (selectedRow != indexPath.row && selectedRow != -1) {
        //deselect previous selected object
            Campaign *previousSelectedC = [self.campaigns objectAtIndex:selectedRow];
            previousSelectedC.selected = @"no";
            [self.campaigns removeObjectAtIndex:selectedRow];
            [self.campaigns insertObject:previousSelectedC atIndex:selectedRow];
        }
        //modify current selected object
        Campaign *currentSelectedC = [self.campaigns objectAtIndex:indexPath.row];
        currentSelectedC.selected = @"yes";
        [self.campaigns removeObjectAtIndex:indexPath.row];
        [self.campaigns insertObject:currentSelectedC atIndex:indexPath.row];
        [self reloadData];
        selectedRow = indexPath.row;
        DetailViewController *vc = (DetailViewController*)[segue destinationViewController];
        [vc setupContent:currentSelectedC];
    }
}
#pragma mark - UINavigationControllerDelegate

- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                          interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>) animationController NS_AVAILABLE_IOS(7_0);
{
    return self.swipeTransition.transitionInProgress?self.swipeTransition:nil;
}

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC  NS_AVAILABLE_IOS(7_0);
{
    if(operation == UINavigationControllerOperationPush){
        NSLog(@"push");
        [self.swipeTransition attachToViewController:toVC];
    }
    else if(operation == UINavigationControllerOperationPop){
        return self.swipeAnimation;
    }
    return nil;
}
#pragma mark - MGSwipeTableCellDelegate

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion;
{
    if (direction == MGSwipeDirectionLeftToRight && index == 0) {
        NSIndexPath * path = [self.tableView indexPathForCell:cell];
        Campaign *c = [self.campaigns objectAtIndex:path.row];
        [self saveToCalendarWithCampain:c];
    }
    return YES;
}
-(void)saveToCalendarWithCampain:(Campaign *)c;
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title =[NSString stringWithFormat:@"Westwing reminder - %@", c.name] ;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        event.startDate = [dateFormatter dateFromString: c.start_time];
        event.endDate = [dateFormatter dateFromString: c.end_time];
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        NSLog(@"%@", err);
        if(!err){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC),dispatch_get_main_queue(),^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create successfully"
                                                                message:@"You just created an event in calendar."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            });
        }
        //self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
    }];
}

#pragma mark - Lazy init
-(NSMutableArray *)campaigns;
{
    if(!_campaigns)
    {
        _campaigns = [NSMutableArray array];
        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
        NSData *storedCampainData = [defaults objectForKey:@"storedCampain"];
        if (storedCampainData) {
            _campaigns = [NSKeyedUnarchiver unarchiveObjectWithData:storedCampainData];
        }
    }
    return _campaigns;
}

- (UILabel *)loadingLabel;
{
    if (!_loadingLabel) {
        _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        _loadingLabel.text = @"Loading...";
        _loadingLabel.textColor = ICONCOLOR;
        _loadingLabel.numberOfLines = 0;
        _loadingLabel.textAlignment = NSTextAlignmentCenter;
        _loadingLabel.font = [UIFont fontWithName:@"BrandonGrotesque-Bold" size:20];
        [_loadingLabel sizeToFit];
        self.tableView.backgroundView = _loadingLabel;
        
    }
    return _loadingLabel;
}

-(SwipeTransition *)swipeTransition;
{
    if (!_swipeTransition) {
        _swipeTransition = [SwipeTransition new];
    }
    return _swipeTransition;
}
-(SwipeAnimation *)swipeAnimation;
{
    if (!_swipeAnimation) {
        _swipeAnimation = [SwipeAnimation new];
    }
    return _swipeAnimation;
}
@end
