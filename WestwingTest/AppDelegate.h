//
//  AppDelegate.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/28/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

