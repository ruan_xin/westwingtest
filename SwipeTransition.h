//
//  SwipeTransition.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/30/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwipeTransition : UIPercentDrivenInteractiveTransition
-(void) attachToViewController:(UIViewController *)viewController;
@property(nonatomic, assign) bool transitionInProgress;
@end
