#WestwingTest Demo
After **git clone**, please open **WestwingTest.xcworkspace** to run this demo.
##Requirement completeness
###Stage 1 - finished
- choose table view
- implement filter function to remove duplicate items.
    + basically use 2 NSMutableArray, one is used for storing campaign name, if already contains same name, ignore this item. Another is used for saving all id_campagin, so that while refresh json data from server, avoid process old campaign.
    + This feature is in `MainTableViewController`->`getLatestCampain`;
- use Aspect fit in storyboard and `ImageView.contentMode = UIViewContentModeScaleAspectFit` in code to keep image with right aspect ratio
- use CABasicAnimation to create a loading circle as the loading indicate, define at `LoadingCircleView.m`

###Stage 2 - finished
- using one customized interactive transaction effect to do the navigate animation between tableview and detailview. `SwipeAnimation.m`,`SwipeTransition.m`, and swipe gesture is triggered by `UIScreenEdgePanGestureRecognizer` at left edge.
- detail screen, for the preview image, considering that maybe late it will have more or less than 3 image, so implement them in a horizontal scrollview. and put this scrollview with text like name, subline, description in another vertical scrollview so that all the content are fully readable.
- create Campaign object to mapping json data, also implement incoder, decoder so that can save to NSUserDefaults for the next time use.
- when user tap a cell, a green tag will showed at the right corner.

###Stage 3 - finished
- nearly every UI element are defined by AutoLayout, with storyboard or programmingly.
- the main animation part is transition effect for the navigation switch and loading circle.
- for the detail view description text use attributed string.

but one confused point is for the requirement 2 - **Change the background of the list whenever the app becomes active**, here for the background, I recognized it as the **background color**. so I use `UIApplicationWillEnterForegroundNotification` and `viewWillAppear` to make sure the table view will change color no matter when it shows from background to foreground or from detailview pop back.

###Test case
for the unit test, since this project's function is not so complicated, also most feature is regarding to UI component, so basically XCTest is not so fit for it. most performance issue can use instrument tools like Allocations, Leaks, Time Profiler to do it.
I simply write 2 assertions test case to check if tableview can correctly initialized and get data. Another one is check the performance of reload data, since in my code, the saving campaigns is also belong to this function, so with the number of local campaigns become larger, it's worth to know how the performance of it.
###Improvement.
on the tableview cell, if swipe left, will show a reminder icon. When user clicked, it will make an event in user's calendar with title **Westwing reminder - campaign name** and lasts from start time to end time. This is a feature I'm thinking maybe useful to user, especially for those events will happen in a few days, a calendar event will bring them back.
>notice: due to the time of sample data is in 2014, so add this event to iPhone calendar will quickly disappeared, but actually it's created and synced to iCloud already, you can also check on your macbook to confirm it.

