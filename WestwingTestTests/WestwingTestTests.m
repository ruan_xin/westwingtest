//
//  WestwingTestTests.m
//  WestwingTestTests
//
//  Created by Xin Ruan on 7/28/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MainTableViewController.h"
@interface MainTableViewController (Test)

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
-(void)reloadData;
@end

@interface WestwingTestTests : XCTestCase
@property(nonatomic,strong) MainTableViewController *mainVCTest;
@end

@implementation WestwingTestTests

- (void)setUp {
    [super setUp];
    self.mainVCTest =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MainTableVC"];
    [self.mainVCTest performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testViewLoads
{
    XCTAssertNotNil(self.mainVCTest.view, @"View not initiated properly");
}
- (void)testTableViewHasDataSource
{
    XCTAssertNotNil(self.mainVCTest.mainTableView.dataSource, @"Table datasource cannot be nil");
}

- (void)testReloadDataPerformance {
    [self measureBlock:^{
        [self.mainVCTest reloadData];
    }];
}


@end
