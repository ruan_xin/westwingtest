//
//  Campaign.h
//  WestwingTest
//
//  Created by Xin Ruan on 7/29/15.
//  Copyright (c) 2015 Xin Ruan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Campaign : NSObject
@property (nonatomic, strong) NSString *id_campaign;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *subline;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *url_key;
@property (nonatomic, strong) NSString *start_time;
@property (nonatomic, strong) NSString *end_time;
@property (nonatomic, strong) NSString *banner_url;
@property (nonatomic, strong) NSString *navigation_url;
@property (nonatomic, strong) UIImage *navi_image;
@property (nonatomic, strong) NSString *end_time_formatted;
@property (nonatomic, strong) NSString *selected;
@property (nonatomic, strong) NSString *start_time_formatted;
@property (nonatomic, strong) NSString *preview_image1_url;
@property (nonatomic, strong) NSString *preview_image2_url;
@property (nonatomic, strong) NSString *preview_image3_url;


@end
